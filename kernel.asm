[org 0x8000] 
[bits 16]

section .text

main_loop:
    in al, 60h
    mov si, keys
    mov cx, 19
    keys_loop:
        mov dl, byte [si]
        cmp al, dl
        je .found
        .not_found:
            inc si
            loop keys_loop
            cmp al, 0
            je end
            jmp main_loop
        .found:
            push ax ; saving al for test in holding key
            mov di, notes
            mov bx, 19 ; bx = cx * 2 (offset)
            sub bx, cx
            shl bx, 1
            mov ax, [word di+bx]
            push ax
            call set_period
            call sound_on
            pop bx
            .hold:
                in al, 60h
                cmp al, bl
                je .continue
                call sound_off
                jmp main_loop
                .continue:
                    jmp .hold

set_period:
    pop bx
    mov al, 0B6h
    out 43h, al
    pop ax
    out 42h, al
    mov al, ah
    out 42h, al
    push bx
    ret

; sound on - in port 61h <- (port 61h or 3)
sound_on:
    in al, 61h
    or al, 03h
    out 61h, al
    ret

; sound off - in port 61h <- (port 61h and 252)
sound_off:
    in al, 61h
    and al, 0FCh
    out 61h, al
    ret

end:
    xor ah, ah
    int 19h

section .data
    keys: db 2Ch, 2Dh, 2Eh, 2Fh, 30h, 31h, 32h
    db 10h, 11h, 12h, 13h, 14h, 15h, 16h, 1Fh, 20h
    db 22h, 23h, 24h, 03h, 04h, 06h, 07h, 08h
    notes: dw 4560, 4063, 3620, 3417, 3044, 2721
    dw 2416, 2280, 2032, 1810, 1708, 1522, 1356, 1208
    dw 4350, 3835, 3225, 2873, 2560, 2152, 1918, 1613
    dw 1437, 1280